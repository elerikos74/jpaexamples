package entity2;

import java.util.Date;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

public class TestStudent {
    private static final String PERSISTENCE_UNIT_NAME = "entity2";
    private static EntityManagerFactory factory;
    private static EntityManager em;
    
    public static void main(String[] args) {
        factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
        em = factory.createEntityManager();
        
        Query q = em.createQuery("SELECT p FROM Student p WHERE p.mId = :ID");
        q.setParameter("ID", 197);
        List lst=q.getResultList();
        
       
        
        Student st=em.find(Student.class, 239L);
        Badge bad=st.getBadge();
        
        em.getTransaction().begin();
        
        Student student = new Student("Lion Hearth", "Richard", new Date());
        Address address = new Address();
        address.setCity("Carouge");
        address.setNumber("7");
        address.setPostalCode("1227");
        address.setStreet("Drize");
        student.setAddress(address);
        Badge b = new Badge();
        b.setSecurityLevel(30L);
        b.setStudent(student);
        student.setBadge(b);
        student.setPhoneNumber(new PhoneNumber(0, 0, 0));
        for (Discipline d : Discipline.values()) {
            Grade g = new Grade(d, 10);
            student.getGrades().add(g);
        }
        student.setPicture("test".getBytes());
        em.persist(student);
        em.getTransaction().commit();
    
    
    }
}
