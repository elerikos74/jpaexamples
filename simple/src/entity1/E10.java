package entity1;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class E10 {
    @Id //signifies the primary key
    @GeneratedValue(strategy = GenerationType.AUTO)  
    private Integer id;
    
    private String name;
    
    @OneToOne(mappedBy = "e10",cascade = CascadeType.ALL)
    private E1 e1;
    
    public E10() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }


    public void setE1(E1 e1) {
        this.e1 = e1;
    }

    public E1 getE1() {
        return e1;
    }
}
