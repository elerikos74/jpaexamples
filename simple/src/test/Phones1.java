package test;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "PHONES")
public class Phones1 {
    @Id
    @Column(nullable = false)
    private Long phoneid;
    private Long phonenumber;
    @ManyToOne
    @JoinColumn(name = "EMPLOYEEID")
    private Employee1 employee1;

    public Phones1() {
    }

    public Phones1(Employee1 employee1, Long phoneid, Long phonenumber) {
        this.employee1 = employee1;
        this.phoneid = phoneid;
        this.phonenumber = phonenumber;
    }


    public Long getPhoneid() {
        return phoneid;
    }

    public void setPhoneid(Long phoneid) {
        this.phoneid = phoneid;
    }

    public Long getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(Long phonenumber) {
        this.phonenumber = phonenumber;
    }

    public Employee1 getEmployee1() {
        return employee1;
    }

    public void setEmployee1(Employee1 employee1) {
        this.employee1 = employee1;
    }
}
