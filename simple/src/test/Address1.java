package test;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "ADDRESS")
public class Address1 {
    private String addresscity;
    private String addresscountry;
    @Id
    @Column(nullable = false)
    private Long addressid;
    @OneToMany(mappedBy = "address1", cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    private List<Employee1> employeeList;

    public Address1() {
    }

    public Address1(String addresscity, String addresscountry, Long addressid) {
        this.addresscity = addresscity;
        this.addresscountry = addresscountry;
        this.addressid = addressid;
    }

    public String getAddresscity() {
        return addresscity;
    }

    public void setAddresscity(String addresscity) {
        this.addresscity = addresscity;
    }

    public String getAddresscountry() {
        return addresscountry;
    }

    public void setAddresscountry(String addresscountry) {
        this.addresscountry = addresscountry;
    }

    public Long getAddressid() {
        return addressid;
    }

    public void setAddressid(Long addressid) {
        this.addressid = addressid;
    }

    public List<Employee1> getEmployeeList() {
        return employeeList;
    }

    public void setEmployeeList(List<Employee1> employeeList) {
        this.employeeList = employeeList;
    }

    public Employee1 addEmployee1(Employee1 employee1) {
        getEmployeeList().add(employee1);
        employee1.setAddress1(this);
        return employee1;
    }

    public Employee1 removeEmployee1(Employee1 employee1) {
        getEmployeeList().remove(employee1);
        employee1.setAddress1(null);
        return employee1;
    }
}
