package javabeat;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity(name = "Project")
public class Project {
    @Id //signifies the primary key
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int projectId;

    private String projectName;

    @ManyToMany(mappedBy="projects",cascade=CascadeType.ALL)
    private List<Employee> employees;
     
    public int getProjectId() {
        return projectId;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public void setEmployees(List employees) {
        this.employees = employees;
    }

    public List getEmployees() {
        return employees;
    }


}
