package test;

import java.io.Serializable;

public class EmployeeProjectPK implements Serializable {
    private Long employee;
    private Long employeeProject;

    public EmployeeProjectPK() {
    }

    public EmployeeProjectPK(Long employee, Long employeeProject) {
        this.employee = employee;
        this.employeeProject = employeeProject;
    }

    public boolean equals(Object other) {
        if (other instanceof EmployeeProjectPK) {
            final EmployeeProjectPK otherEmployeeProjectPK = (EmployeeProjectPK) other;
            final boolean areEqual =
                (otherEmployeeProjectPK.employee.equals(employee) &&
                 otherEmployeeProjectPK.employeeProject.equals(employeeProject));
            return areEqual;
        }
        return false;
    }

    public int hashCode() {
        return super.hashCode();
    }

    public Long getEmployee() {
        return employee;
    }

    public void setEmployee(Long employee) {
        this.employee = employee;
    }

    public Long getEmployeeProject() {
        return employeeProject;
    }

    public void setEmployeeProject(Long employeeProject) {
        this.employeeProject = employeeProject;
    }
}
