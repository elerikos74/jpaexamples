package entity1;

import java.util.Collection;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class E3 {
    @Id //signifies the primary key
    @GeneratedValue(strategy = GenerationType.AUTO)  
    private Integer id;
    
    @Basic(optional = false)
    private String name;
    
    @OneToMany(mappedBy="e3",cascade=CascadeType.ALL)
    private Collection<E1> e1list;

    public E3() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public void setE1list(Collection<E1> e1list) {
        this.e1list = e1list;
    }

    public Collection<E1> getE1list() {
        return e1list;
    }


    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }


}
