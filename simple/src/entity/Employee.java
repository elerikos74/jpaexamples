package entity;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity(name = "EMPLOYEES") 
public class Employee {
    @Id //signifies the primary key
    @GeneratedValue(strategy = GenerationType.AUTO)   
    private int id;
   
    private String salary;
    
    @OneToOne(cascade=CascadeType.ALL)
    private Person person;
    
    @ManyToOne(cascade=CascadeType.ALL)
    private Department department;
    
    @ManyToOne(cascade=CascadeType.ALL)
    private Company company;
    
    @ManyToMany(cascade=CascadeType.ALL)
    @JoinTable(name="EMP_PROJ",
              joinColumns=@JoinColumn(name="EMP_ID"),
              inverseJoinColumns=@JoinColumn(name="PROJ_ID"))
        private Collection<Project> projects =new ArrayList<Project>();
    
    
    public Employee() {
        super();
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }


    public void setSalary(String salary) {
        this.salary = salary;
    }

    public String getSalary() {
        return salary;
    }


    public void setDepartment(Department department) {
        this.department = department;
    }

    public Department getDepartment() {
        return department;
    }

    public void setProjects(Collection projects) {
        this.projects = projects;
    }

    public Collection getProjects() {
        return projects;
    }


    public void setCompany(Company company) {
        this.company = company;
    }

    public Company getCompany() {
        return company;
    }


    public void setPerson(Person person) {
        this.person = person;
    }

    public Person getPerson() {
        return person;
    }
    
    public void addProject(Project project){
        this.projects.add(project);
    }

}
