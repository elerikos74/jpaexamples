package de.vogella.jpa.simple.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="COUNTRIES")
public class Country{
private String id;
private String name;
private Region region;

@Id
@Column(name="COUNTRY_ID")
public String getId() {
return id;
}

public void setId(String id) {
this.id = id;
}

@Column(name="COUNTRY_NAME")
public String getName() {
return name;
}

public void setName(String name) {
this.name = name;
}

@ManyToOne()
@JoinColumn(name="REGION_ID",nullable=false)
public Region getRegion() {
return region;
}

public void setRegion(Region region) {
this.region = region;
}
}
