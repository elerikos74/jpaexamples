package javabeat;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value="DEV") // Notice using of discriminatorValue again
public class Developer extends Employee{

 private String title;

public String getTitle() {
 return title;
 }

public void setTitle(String title) {
 this.title = title;
 }

}
