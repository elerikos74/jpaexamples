package entity1;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class E6 {
    @Id //signifies the primary key
    @GeneratedValue(strategy = GenerationType.AUTO)  
    private Integer id;
    
    @Basic(optional = false)
    private String name;
    
    @OneToOne(cascade=CascadeType.ALL,mappedBy="e6")
    private E5 e5;

    public E6() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public void setE5(E5 e5) {
        this.e5 = e5;
    }

    public E5 getE5() {
        return e5;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
