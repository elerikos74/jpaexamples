package entity1;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.Id;
import javax.persistence.IdClass;

@Embeddable
public class E22ID implements Serializable{

    private Integer e1;
    private String name;

    public void setE1(Integer e1) {
        this.e1 = e1;
    }

    public Integer getE1() {
        return e1;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
    
    public int hashCode() {
           return (int)this.name.hashCode()+ this.e1;
       }
    
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (!(obj instanceof EmployeePK)) return false;
        E22ID pk = (E22ID) obj;
        return pk.e1 == this.e1 && pk.name.equals(this.name);
    }


    public E22ID(){}
    
    
}
