package javabeat;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

public class Main {
    private static final String PERSISTENCE_UNIT_NAME = "javabeat";
    private static EntityManagerFactory factory;
    private static EntityManager em;


    public static void main(String[] args) {
        factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
        em = factory.createEntityManager();
        em.getTransaction().begin();

        //   createEmployee();
        createEmployee();
        createDeveloper();
        em.getTransaction().commit();
        
        Query query=em.createQuery("select distinct(t.employeeId) from Employee t");
        
        List ert=query.getResultList();
        
        Employee emp = em.find(Employee.class, 1);
        if (emp!=null) {
            String country=emp.getAddress().getAddressCountry(); 
            int a =emp.getPhones().get(0).getPhoneNumber();
        }
       
       
        Boolean b=null;
    }
    public static void createDeveloper(){
     // Create an address entity
     Address address = new Address();
     address.setAddressCountry("United Kingdom");
     address.setAddressCity("London");
     // Create an employee entity
     Developer developer = new Developer();
     developer.setEmployeeName("John Smith");
     developer.setTitle("Senior Java Developer");
     // Associate the address with the employee
     developer.setAddress(address);
     // Create a Phone entity
     Phone firstPhone = new Phone();
     firstPhone.setPhoneNumber(34523);
     firstPhone.setEmployee(developer);
     // Create a new phone entity
     Phone secondPhone = new Phone();
     secondPhone.setPhoneNumber(3242);
     // Use the old employee entity
     secondPhone.setEmployee(developer);
     // Create a list of phone
     List<Phone> phones = new ArrayList<Phone>();
     phones.add(firstPhone);
     phones.add(secondPhone);

     // Create a Project entity
     Project project = new Project();
     project.setProjectName("Nasa Project1");

     // Create a list of projects
     List<Project> projects = new ArrayList<Project>();

     // add the project into the list
     projects.add(project);

     // Set the project into employee
     developer.setProjects(projects);
     // Set the phones into your employee
     developer.setPhones(phones);
     // Persist the employee
     em.persist(developer);

     }

    public static void createEmployee() {
        // Create an address entity
        Address address = new Address();

        address.setAddressCountry("United Kingdom");
        address.setAddressCity("London");
        // Create an employee entity
        Employee employee = new Employee();
        employee.setEmployeeName("John Smith");
        // Associate the address with the employee
        employee.setAddress(address);
        // Create a Phone entity
        Phone firstPhone = new Phone();
        firstPhone.setPhoneNumber(8667);
        firstPhone.setEmployee(employee);
        // Create a new phone entity
        Phone secondPhone = new Phone();
        secondPhone.setPhoneNumber(67577575);
        // Use the old employee entity
        secondPhone.setEmployee(employee);
        // Create a list of phone
        List<Phone> phones = new ArrayList<Phone>();
        phones.add(firstPhone);
        phones.add(secondPhone);

        // Create a Project entity
        Project project = new Project();
        project.setProjectName("Nasa Project");

        // Create a list of projects
        List<Project> projects = new ArrayList<Project>();

        // add the project into the list
        projects.add(project);

        // Set the project into employee
        employee.setProjects(projects);
        // Set the phones into your employee
        employee.setPhones(phones);

        // Persist the employee

        em.persist(employee);
    }
}
