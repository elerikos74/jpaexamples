package entity1;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class E5 {
    @Id //signifies the primary key
    @GeneratedValue(strategy = GenerationType.AUTO)  
    private Integer id;
    
    @Basic(optional = false)
    private String name;
    
    @OneToOne(cascade=CascadeType.ALL,mappedBy="e5",targetEntity=E1.class,optional=false)
    private E1 e1;
    
    @OneToOne(cascade=CascadeType.ALL,fetch=FetchType.LAZY)
    private E6 e6;

    public E5() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public void setE1(E1 e1) {
        this.e1 = e1;
    }

    public E1 getE1() {
        return e1;
    }


    public void setE6(E6 e6) {
        this.e6 = e6;
    }

    public E6 getE6() {
        return e6;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
