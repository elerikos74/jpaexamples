package entity2;

import java.io.InputStream;

import java.util.Date;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import javax.xml.bind.Marshaller;


import org.eclipse.persistence.jaxb.JAXBMarshaller;

public class TestMarshal {
    public static void main(String[] args) {
        Student student = new Student("Lion Hearth", "Richard", new Date());
        Address address = new Address();
        address.setCity("Carouge");
        address.setNumber("7");
        address.setPostalCode("1227");
        address.setStreet("Drize");
        student.setAddress(address);
        Badge b = new Badge();
        b.setSecurityLevel(30L);
        b.setStudent(student);
        student.setBadge(b);
        student.setPhoneNumber(new PhoneNumber(0, 0, 0));
        for (Discipline d : Discipline.values()) {
            Grade g = new Grade(d, 10);
            student.getGrades().add(g);
        }

        try {
            JAXBContext jaxbContext =  JAXBContext.newInstance(Student.class);
            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(student, System.out);

        } catch (JAXBException e) {
        }
        
   
    }
    
    public TestMarshal() {
        super();
    }
}
