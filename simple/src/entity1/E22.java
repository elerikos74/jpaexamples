package entity1;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.OneToOne;

@Entity
//@IdClass(E22ID.class)
public class E22 {
    
    @EmbeddedId
    E22ID primaryKey;
    
//    @Id     
//    @OneToOne(cascade={CascadeType.ALL})    
//    private E1 e1;
//    
//    @Id
//    private String name;


//    public void setE1(E1 e1) {
//        this.e1 = e1;
//    }
//
//    public E1 getE1() {
//        return e1;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }
//
//    public String getName() {
//        return name;
//    }


    public void setPrimaryKey(E22ID primaryKey) {
        this.primaryKey = primaryKey;
    }

    public E22ID getPrimaryKey() {
        return primaryKey;
    }

    public E22(){super();}

//    public E22(E1 e1,String name) {
//        
//        this.e1=e1;
//        this.name=name;
//    }
}
