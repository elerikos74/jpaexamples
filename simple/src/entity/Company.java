package entity;

import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="COMPANIES")
public class Company {
    
    @Id //signifies the primary key
    @GeneratedValue(strategy = GenerationType.AUTO)   
    private int id;
    
    private String name;
    
    @OneToMany(mappedBy="company",fetch=FetchType.EAGER)
    private Collection<Employee> employees;
    
    public Company() {
        super();
    }


    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }


    public void setEmployees(Collection<Employee> employees) {
        this.employees = employees;
    }

    public Collection<Employee> getEmployees() {
        return employees;
    }


    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
