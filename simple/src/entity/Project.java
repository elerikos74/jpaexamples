package entity;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

@Entity(name = "PROJECTS") 
public class Project {
    @Id //signifies the primary key
    @GeneratedValue(strategy = GenerationType.AUTO)   
    private int id;
    
    private String name;
     
    @ManyToMany(mappedBy = "projects")
    private Collection<Employee> employees =new ArrayList<Employee>();
    
    public Project() {
        super();
    }
    
    public Project(String name) {
        this.name=name;
    }


    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }


    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }


    public void setEmployees(Collection employees) {
        this.employees = employees;
    }

    public Collection getEmployees() {
        return employees;
    }


}
