package entity1;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

@Entity
public class Employee implements Serializable {
 
    @EmbeddedId
    EmployeePK primaryKey;
 
    public Employee() {
    }
 
    public EmployeePK getPrimaryKey() {
        return primaryKey;
    }
 
    public void setPrimaryKey(EmployeePK pk) {
        primaryKey = pk;
    }
 
    
}