package entity;

import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity(name="CHILDREN")
public class Child {
    
    @Id //signifies the primary key
    @GeneratedValue(strategy = GenerationType.AUTO)   
    private int id;
    
    private String name;
    
    private Person parent;
    
    public Child() {
        super();
    }


    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }


    public void setParent(Person parent) {
        this.parent = parent;
    }

    public Person getParent() {
        return parent;
    }
}
