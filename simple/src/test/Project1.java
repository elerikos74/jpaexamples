package test;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "PROJECT")
public class Project1 {
    @Id
    @Column(nullable = false)
    private Long projectid;
    private String projectname;
    @OneToMany(mappedBy = "employeeProject", cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    private List<EmployeeProject> employeeProjectList1;

    public Project1() {
    }

    public Project1(Long projectid, String projectname) {
        this.projectid = projectid;
        this.projectname = projectname;
    }

    public Long getProjectid() {
        return projectid;
    }

    public void setProjectid(Long projectid) {
        this.projectid = projectid;
    }

    public String getProjectname() {
        return projectname;
    }

    public void setProjectname(String projectname) {
        this.projectname = projectname;
    }

    public List<EmployeeProject> getEmployeeProjectList1() {
        return employeeProjectList1;
    }

    public void setEmployeeProjectList1(List<EmployeeProject> employeeProjectList1) {
        this.employeeProjectList1 = employeeProjectList1;
    }

    public EmployeeProject addEmployeeProject(EmployeeProject employeeProject) {
        getEmployeeProjectList1().add(employeeProject);
        employeeProject.setEmployeeProject(this);
        return employeeProject;
    }

    public EmployeeProject removeEmployeeProject(EmployeeProject employeeProject) {
        getEmployeeProjectList1().remove(employeeProject);
        employeeProject.setEmployeeProject(null);
        return employeeProject;
    }
}
