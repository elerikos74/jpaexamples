package de.vogella.jpa.simple.main;

import de.vogella.jpa.eclipselink.model.Family;

import de.vogella.jpa.eclipselink.model.Person2;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import de.vogella.jpa.simple.model.Todo;

import entity.Child;
import entity.Company;
import entity.Department;
import entity.Employee;
import entity.Person;
import entity.Project;

import entity1.E1;
import entity1.E2;
import entity1.E3;

import entity1.E4;

import entity1.E5;

import entity1.E6;



import java.util.ArrayList;


public class Main {
  private static final String PERSISTENCE_UNIT_NAME = "todos";
  private static EntityManagerFactory factory;

  public static void main(String[] args) {
    factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
    EntityManager em = factory.createEntityManager();
    
    // read the existing entries and write to console
  //  Query q = em.createQuery("select t from Todo t");
   //   Query q1 = em.createQuery("DELETE FROM Todo");

   em.getTransaction().begin();
   E1 e1=new E1();
   E2 e2=new E2();
   E3 e3=new E3();
      E5 e5=new E5();
      E6 e6=new E6();
   //E4 e4=new E4();
   ArrayList<E4> arr=new ArrayList<E4>();
   arr.add(new E4());
      arr.add(new E4());
      e1.setE4list(arr);
   
   e1.setE3(e3);
      e5.setE6(e6);
   e1.setE5(e5);
      e2.setE1(e1);
   em.persist(e2);
   //em.persist(e1);
   em.getTransaction().commit();
      System.exit(0);  
   
   
   Employee emp=new Employee();
   Person person=new Person();
      person.setName("erik");
      emp.setPerson(person);
      Child child=new Child();
      child.setName("john");
      child.setParent(person);
      
      Company company=new Company();
      company.setName("Siemens");
      Department dep=new Department();
      dep.setName("HR");
      Project prj1=new Project("ethink");
      Project prj2=new Project("lf");
      emp.addProject(prj1);
      emp.addProject(prj2);
      emp.setDepartment(dep);
      emp.setCompany(company);
      emp.setSalary("80000");
      em.persist(emp);
      em.persist(child);
      em.getTransaction().commit();
      System.exit(0);  
   //manyToOne


   Query q = em.createQuery("select t from Todo t");
     


      
      
   //OneToOne
      Department dep1 =  new Department();


      System.exit(0);
//ManyToOne   
      dep1.setName("dep1");
      Employee emp1= new Employee();
      emp1.setDepartment(dep1);
      emp1.setSalary("8098");
    //  em.persist(dep1);
      em.persist(emp1);
      em.getTransaction().commit();
      System.exit(0);
      
    List<Todo> todoList = q.getResultList();
    for (Todo todo : todoList) {
      System.out.println(todo);
    }
    System.out.println("Size: " + todoList.size());

      Long tmp=0L;
      
    if (false){
        em.getTransaction().begin();
        Todo todo = new Todo();
        todo.setSummary("This is a test");
        todo.setDescription("This is a test");
        em.persist(todo);
        
        Todo todo1 = new Todo();
        todo1.setSummary("This is a test1");
        todo1.setDescription("This is a test1");
        em.persist(todo1);   
        em.getTransaction().commit();
        tmp=todo1.getId();
    }

 //   em.getTransaction().begin();
  //  em.clear();
    
    
    Todo todo2=em.find(Todo.class,tmp);
    
      Todo todo3=em.find(Todo.class,new Long(97));
    
    if (todo2!=null)
      System.out.println("todo2: " + todo2.getId());
    if (todo3!=null)
      System.out.println("todo3: " + todo3.getId());
   // em.getTransaction().commit();
      
      
      
   // Begin a new local transaction so that we can persist a new entity
       em.getTransaction().begin();

       // read the existing entries
       q = em.createQuery("select m from Person m");
       // Persons should be empty

       // do we have entries?
       int createNewEntries = q.getResultList().size();

       // No, so lets create new entries
       if (createNewEntries==0) {
         boolean b=(q.getResultList().size() == 0);
         Family family = new Family();
         family.setDescription("Family for the Knopfs1");
         //em.persist(family);
         for (int i = 0; i < 40; i++) {
           Person2 person2 = new Person2();
           person2.setFirstName("Jim1_" + i);
           person2.setLastName("Knop1f_" + i);
          // em.persist(person);
           // now persists the family person relationship
      //     family.getMembers().add(person2);
          // em.persist(person);
          // em.persist(family);
         }
           em.persist(family);
       }

       // Commit the transaction, which will cause the entity to
       // be stored in the database
       em.getTransaction().commit();
       
       
       q = em.createQuery("select m from Person m");

        // We should have 40 Persons in the database
        int size=q.getResultList().size();
        
        
        
      em.getTransaction().begin();
          q = em.createQuery("SELECT p FROM Person p WHERE p.firstName = :firstName AND p.lastName = :lastName");
          q.setParameter("firstName", "Jim_2");
          q.setParameter("lastName", "Knopf_3");
          List lst=q.getResultList();
          if (lst.size()>0){
          Person2 user = (Person2) q.getSingleResult();
          em.remove(user);
          }
          em.getTransaction().commit();
          if (q.getResultList().size()>0){
              Person2 person2 = (Person2) q.getSingleResult();
              int o=0;
          }
           
      
    em.close();
  }
} 