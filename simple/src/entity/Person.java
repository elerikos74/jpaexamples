package entity;

import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity(name = "PERSONS")
public class Person {
    @Id //signifies the primary key
    @GeneratedValue(strategy = GenerationType.AUTO) 
    private int id;
    
    private String name;
    
    @OneToMany(mappedBy="parent")
    private Collection<Child> childs;
    
    @OneToOne(mappedBy="person")
    private Employee employee;
    
    
    public Person() {
        super();
    }


    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }


    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Employee getEmployee() {
        return employee;
    }


    public void setChilds(Collection<Child> childs) {
        this.childs = childs;
    }

    public Collection<Child> getChilds() {
        return childs;
    }

}
