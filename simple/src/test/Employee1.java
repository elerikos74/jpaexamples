package test;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "EMPLOYEE")
public class Employee1 {
    @Id
    @Column(nullable = false)
    private Long employeeid;
    private String employeename;
    @Column(length = 31)
    private String employeetype;
    private String title;
    @OneToMany(mappedBy = "employee", cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    private List<EmployeeProject> employeeProjectList;
    @ManyToOne
    @JoinColumn(name = "ADDRESSID")
    private Address1 address1;
    @OneToMany(mappedBy = "employee1", cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    private List<Phones1> phonesList;

    public Employee1() {
    }

    public Employee1(Address1 address1, Long employeeid, String employeename, String employeetype, String title) {
        this.address1 = address1;
        this.employeeid = employeeid;
        this.employeename = employeename;
        this.employeetype = employeetype;
        this.title = title;
    }


    public Long getEmployeeid() {
        return employeeid;
    }

    public void setEmployeeid(Long employeeid) {
        this.employeeid = employeeid;
    }

    public String getEmployeename() {
        return employeename;
    }

    public void setEmployeename(String employeename) {
        this.employeename = employeename;
    }

    public String getEmployeetype() {
        return employeetype;
    }

    public void setEmployeetype(String employeetype) {
        this.employeetype = employeetype;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<EmployeeProject> getEmployeeProjectList() {
        return employeeProjectList;
    }

    public void setEmployeeProjectList(List<EmployeeProject> employeeProjectList) {
        this.employeeProjectList = employeeProjectList;
    }

    public EmployeeProject addEmployeeProject(EmployeeProject employeeProject) {
        getEmployeeProjectList().add(employeeProject);
        employeeProject.setEmployee(this);
        return employeeProject;
    }

    public EmployeeProject removeEmployeeProject(EmployeeProject employeeProject) {
        getEmployeeProjectList().remove(employeeProject);
        employeeProject.setEmployee(null);
        return employeeProject;
    }

    public Address1 getAddress1() {
        return address1;
    }

    public void setAddress1(Address1 address1) {
        this.address1 = address1;
    }

    public List<Phones1> getPhonesList() {
        return phonesList;
    }

    public void setPhonesList(List<Phones1> phonesList) {
        this.phonesList = phonesList;
    }

    public Phones1 addPhones1(Phones1 phones1) {
        getPhonesList().add(phones1);
        phones1.setEmployee1(this);
        return phones1;
    }

    public Phones1 removePhones1(Phones1 phones1) {
        getPhonesList().remove(phones1);
        phones1.setEmployee1(null);
        return phones1;
    }
}
