package test;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "EMPLOYEE_PROJECT")
@IdClass(EmployeeProjectPK.class)
public class EmployeeProject {
    @ManyToOne
    @Id
    @JoinColumn(name = "EMPLOYEEID")
    private Employee1 employee;
    @ManyToOne
    @Id
    @JoinColumn(name = "PROJECTID")
    private Project1 employeeProject;

    public EmployeeProject() {
    }

    public EmployeeProject(Employee1 employee, Project1 employeeProject) {
        this.employee = employee;
        this.employeeProject = employeeProject;
    }


    public Employee1 getEmployee() {
        return employee;
    }

    public void setEmployee(Employee1 employee) {
        this.employee = employee;
    }

    public Project1 getEmployeeProject() {
        return employeeProject;
    }

    public void setEmployeeProject(Project1 employeeProject) {
        this.employeeProject = employeeProject;
    }
}
