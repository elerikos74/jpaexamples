package entity1;

import javax.persistence.Basic;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class E9 extends E1{
    @Id
    private Integer iddd;
    
    @Basic(optional = false)
    private String e9name;

    public E9() {
    }


    public void setIdd(Integer iddd) {
        this.iddd = iddd;
    }

    public Integer getIdd() {
        return iddd;
    }


    public void setE9name(String e9name) {
        this.e9name = e9name;
    }

    public String getE9name() {
        return e9name;
    }

}
