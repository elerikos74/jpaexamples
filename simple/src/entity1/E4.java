package entity1;

import entity.Employee;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity
public class E4 {
    @Id //signifies the primary key
    @GeneratedValue(strategy = GenerationType.AUTO)  
    private Integer id;
    
    @Basic(optional = false)
    private String name;
    
    @ManyToMany(mappedBy = "e4list")
    private Collection<E1> e1list =new ArrayList<E1>();

    public E4() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public void setE1list(Collection<E1> e1list) {
        this.e1list = e1list;
    }

    public Collection<E1> getE1list() {
        return e1list;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
