package entity1;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SecondaryTable;

@Entity
@SecondaryTable(name = "E9", pkJoinColumns = @PrimaryKeyJoinColumn(name = "IDDD", referencedColumnName = "ID"))
//@Inheritance(strategy = InheritanceType.JOINED)
//@DiscriminatorColumn(name = "E_TYPE")
public class E1 {
    @Id //signifies the primary key
    @GeneratedValue(strategy = GenerationType.AUTO)  
    private Integer id;
    
    @Basic(optional = false)
    @Column(length=10)
    private String name;
    
    @Basic(optional = false)
    private char cha;
    
    @Enumerated(EnumType.STRING)
    private Enum1 fname;
    
    @OneToMany(mappedBy="e1",cascade=CascadeType.ALL)
    private Collection<E2> e2list;
    
    @OneToMany(cascade=CascadeType.ALL)
    private Collection<E7> e7list;
    
    @ManyToOne(cascade=CascadeType.ALL)
    @JoinColumn(name = "E3_ID")
    private E3 e3;
    
    @ManyToMany(cascade=CascadeType.ALL)
    private Collection<E4> e4list =new ArrayList<E4>();
    
    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name = "E5_ID")
    private E5 e5;
    
    @OneToOne(cascade = CascadeType.ALL)
    private E10 e10;

    public E1() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public void setE2list(Collection<E2> e2list) {
        this.e2list = e2list;
    }

    public Collection<E2> getE2list() {
        return e2list;
    }


    public void setE3(E3 e3) {
        this.e3 = e3;
    }

    public E3 getE3() {
        return e3;
    }


    public void setE4list(Collection<E4> e4list) {
        this.e4list = e4list;
    }

    public Collection<E4> getE4list() {
        return e4list;
    }

    public void setE5(E5 e5) {
        this.e5 = e5;
    }

    public E5 getE5() {
        return e5;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }


    public void setFname(Enum1 fname) {
        this.fname = fname;
    }

    public Enum1 getFname() {
        return fname;
    }


    public void setE7list(Collection<E7> e7list) {
        this.e7list = e7list;
    }

    public Collection<E7> getE7list() {
        return e7list;
    }


    public void setCha(char cha) {
        this.cha = cha;
    }

    public char getCha() {
        return cha;
    }


    public void setE10(E10 e10) {
        this.e10 = e10;
    }

    public E10 getE10() {
        return e10;
    }

}
