package de.vogella.jpa.simple.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;

import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="REGIONS")
public class Region{
private int id;
private String name;
private List<Country> countries = new ArrayList<Country>();;

@Id
@Column(name="REGION_ID")
public int getId() {
return id;
}

public void setId(int id) {
this.id = id;
}

@Column(name="REGION_NAME")
public String getName() {
return name;
}

public void setName(String name) {
this.name = name;
}

@OneToMany(cascade={CascadeType.ALL}, mappedBy="region")
public List<Country> getCountries() {
return countries;
}

public void setCountries(List<Country> newValue) {
this.countries = newValue;
}
}
