package entity1;

import java.io.Serializable;

import javax.persistence.Embeddable;


@Embeddable
public class EmployeePK implements Serializable {
 
    private String empName;
    private long empID;
 
    public EmployeePK() {
    }
 
    public String getName() {
        return this.empName;
    }
 
    public void setName(String name) {
        this.empName = name;
    }
 
    public long getEmpID() {
        return this.empID;
    }
 
    public void setEmpID(long id) {
        this.empID = id;
    }
 
    public int hashCode() {
        return (int) (this.empName.hashCode() + this.empID);
    }
 
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (!(obj instanceof EmployeePK)) return false;
        EmployeePK pk = (EmployeePK) obj;
        return pk.empID == this.empID && pk.empName.equals(this.empName);
    }
}