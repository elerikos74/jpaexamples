package entity1;

import javax.persistence.Basic;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
//@DiscriminatorValue(value="ert")
public class E8 extends E1{
    @Id
    private Integer idd;
    
    @Basic(optional = false)
    private String e8name;

    public E8() {
    }


    public void setIdd(Integer idd) {
        this.idd = idd;
    }

    public Integer getIdd() {
        return idd;
    }


    public void setE8name(String e8name) {
        this.e8name = e8name;
    }

    public String getE8name() {
        return e8name;
    }
}
